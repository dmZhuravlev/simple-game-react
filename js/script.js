/**
 * first React element (children)
 */
var Tile = React.createClass({
    /**
     * onClick handler
     */
    clicked: function () {
        this.props.checking(this.props.val, this.props.ind, this.props.act);
    },
    mouseDw: function () {
        this.props.prevSelect(this.props.ind, !this.props.act);
    },
    /**
     * render React method
     */
    render: function () {
        var classNm = '';
        var elNumber;
        if (this.props.matchFlg) {
            classNm = 'matched elem-st';
            elNumber = this.props.val;
        } else {
            classNm = this.props.act ? 'active elem-st' : 'elem-st';
            elNumber = this.props.act ? this.props.val : '';
        }
        return  (<p className={ classNm }
            onMouseDown={this.mouseDw}
            onClick={this.clicked}>
                        {elNumber}
        </p>);
    }
});
/**
 * second React element (parent)
 */
var ListOfTiles = React.createClass({
    /**
     * React method
     * set default property for variables, which we will use in this react class
     */
    getInitialState: function () {
        return {
            total: 12,
            shuffleArr: null
        }
    },
    /**
     * onClick handler function
     */
    checking: function (value, index, act) {
        var self = this;
        var counter = 0;

        if (this.state.shuffleArr[index].matchFlg) {
            return;
        }

        var checkingSameElement = function(elem, ind) {
            if (self.state.shuffleArr[ind].active === true && !self.state.shuffleArr[ind].matchFlg) {
                counter+=1;
            }
            if (value === elem.val &&
                self.state.shuffleArr[index].active === true &&
                self.state.shuffleArr[ind].active === true &&
                index !== ind) {

                self.state.shuffleArr[ind].matchFlg = true;
                self.state.shuffleArr[index].matchFlg = true;
                //checkSameElemFlag = true;
            }
        };
        var closeAllElements = function (elm, ind) {
            if (self.state.shuffleArr[ind].active === true) {
                self.state.shuffleArr[ind].active = false;
            }
        };
        this.state.shuffleArr.forEach(checkingSameElement);

        if (counter >= 2) {
            this.state.shuffleArr.forEach(closeAllElements);
        }
        /**
         * timeout for matching effect
         */
        setTimeout(function () {
            /**
             * in this place we repaint our (view) or (react element)
             * we set changing array and React repaint element, only if we use setState
             */
            self.setState({ shuffleArr: self.state.shuffleArr });
        }, 200)

    },
    /**
     * custom method
     * blending or shuffle array
     * @param arr
     * @returns {Array}
     */
    shuffle: function (arr) {
        var j;
        var x;
        for (var i = arr.length; i;) {
            j = Math.floor(Math.random() * i);
            x = arr[--i];
            arr[i] = arr[j];
            arr[j] = x;
        }
        return arr;
    },
    /**
     * custom method
     * create concatenate array
     * for example (1-10) and (1-10)
     * @param arrLen
     * @returns {Array}
     */
    createArr: function (arrLen) {
        var arr = [];

        for (var i = 0; i < arrLen; i++) {
            arr[i] = i;
        }
        arr = arr.concat(arr.slice(0));
        return arr;
    },
    /**
     * fix prevent selection of element
     * @returns {boolean}
     */
    prevSelect: function (ind, act) {
        /**
         * fix prevent selection of element
         * @returns {boolean}
         */
        this.getDOMNode().onmousedown = function () {
            return false;
        };
        this.getDOMNode().onselectstart = function () {
            return false;
        };
        /**
         *
         */
        if (!act) {
            return;
        }
        this.state.shuffleArr[ind].active = act;
        this.setState({ shuffleArr: this.state.shuffleArr });

    },
    /**
     * render React method
     * @returns {XML}
     */
    render: function () {
        var self = this;

        if (!this.state.shuffleArr) {
            var shuffleArr = this.shuffle(this.createArr(this.state.total));

            var resultArr = [];
            for (var i = 0; i < shuffleArr.length; i++) {
                var objEl = {};
                objEl.val = shuffleArr[i];
                objEl.active = false;
                resultArr.push(objEl);
            }

            this.state.shuffleArr = resultArr;
        }

        var tiles = this.state.shuffleArr.map(function (elem, ind) {
            return <Tile val={elem.val} ind={ind} act={elem.active} matchFlg={elem.matchFlg} prevSelect={self.prevSelect} checking={self.checking} />;
        });
        return (<div className='wrap-all'> {tiles} </div>);
    }
});
React.render(
    <ListOfTiles  />,
    document.getElementById('content')
);
/**
 * fix prevent selection of element
 * @returns {boolean}
 */
document.getElementById('content').onselectstart = function () {
    return false;
};